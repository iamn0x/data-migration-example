from django.db import models

from utils.db.fields import PhoneField
from utils.db.models import GDPRConsentModel, TimeCreationModel


class Subscriber(GDPRConsentModel, TimeCreationModel, models.Model):
    email = models.EmailField(unique=True)


class SubscriberSMS(GDPRConsentModel, TimeCreationModel, models.Model):
    phone = PhoneField(unique=True)
