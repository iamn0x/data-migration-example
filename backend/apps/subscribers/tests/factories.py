import factory
from factory import fuzzy

from apps.subscribers.models import Subscriber, SubscriberSMS


class SubscriberFactory(factory.django.DjangoModelFactory):
    email = factory.Faker("email")
    gdpr_consent = fuzzy.FuzzyChoice([True, False])

    class Meta:
        model = Subscriber


class SubscriberSMSFactory(factory.django.DjangoModelFactory):
    gdpr_consent = fuzzy.FuzzyChoice([True, False])
    phone = fuzzy.FuzzyInteger(100000000, 999999999)

    class Meta:
        model = SubscriberSMS
