# Generated by Django 4.0 on 2022-02-17 15:26

from django.db import migrations, models

import utils.db.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Subscriber",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("gdpr_consent", models.BooleanField(verbose_name="General Data Protection Regulation consent")),
                ("created_date", models.DateTimeField(auto_now_add=True, verbose_name="Date of creation")),
                ("email", models.EmailField(max_length=254, unique=True)),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.CreateModel(
            name="SubscriberSMS",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("gdpr_consent", models.BooleanField(verbose_name="General Data Protection Regulation consent")),
                ("created_date", models.DateTimeField(auto_now_add=True, verbose_name="Date of creation")),
                ("phone", utils.db.fields.PhoneField(max_length=15, unique=True)),
            ],
            options={
                "abstract": False,
            },
        ),
    ]
