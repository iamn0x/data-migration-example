from django.db import models

from utils.db.fields import PhoneField
from utils.db.models import TimeCreationModel


class Client(TimeCreationModel, models.Model):
    email = models.EmailField(unique=True)
    phone = PhoneField()
