import factory
from factory import fuzzy

from apps.customers.models import Client


class ClientFactory(factory.django.DjangoModelFactory):
    email = factory.Faker("email")
    phone = fuzzy.FuzzyInteger(100000000, 999999999)

    class Meta:
        model = Client
