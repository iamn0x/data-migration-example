import logging
import os

from django.conf import settings
from django.core.management.base import BaseCommand
from django.db.models import Count, Exists, OuterRef, Subquery
from django.db.models.functions import Coalesce

from apps.customers.models import Client
from apps.subscribers.models import Subscriber, SubscriberSMS
from apps.users.models import User
from utils.helpers import save_objs_to_csv

# pylint: skip-file

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = """Migrates Subscriber and SubscriberSMS data to User table

        - Omits Clients with duplicated phone number
            - Omits Subscribers whose e-mails exist in the User table

                - If there's Client.email == Subscriber.email
                and not exists User.phone == Client.phone
                and User.email != Client.email -> Creates new User based on Client model

                - If there's Client.email == Subscriber.email
                and User.phone == Client.phone
                and User.email != Client.email -> Saves Subscriber ID and email to 'subscriber_conflicts.csv'

                - If there's not Client.email == Subscriber.email -> Creates new User without phone

            - Omits SubscriberSMS whose phones exist in the User table

                - If there's Client.phone == Subscriber.phone
                and not exists User.email == Client.email
                and User.phone != Client.phone -> Creates new User based on Client model

                - If there's Client.phone == Subscriber.phone
                and User.email == Client.email
                and User.phone != Client.phone -> Saves Subscriber ID and phone to 'subscriber_sms_conflicts.csv'

                - If there's not Client.phone == Subscriber.phone -> Creates new User without email
        """

    def handle(self, *args, **options):
        if not os.path.exists(settings.REPORTS_ROOT):
            os.mkdir(settings.REPORTS_ROOT)

        duplicated_client_phones = []
        users_to_create = []
        subscriber_conflicts = []
        subscriber_sms_conflicts = []

        client_phones_with_phone_count = (
            Client.objects.filter(phone=OuterRef("phone")).values("phone").annotate(phone_count=Count("*"))
        )
        subscriber_emails = Subscriber.objects.filter(email=OuterRef("email"))
        subscriber_sms_phones = SubscriberSMS.objects.filter(phone=OuterRef("phone"))
        user_phones = User.objects.filter(phone=OuterRef("phone"))
        user_emails = User.objects.filter(email=OuterRef("email"))

        clients_to_migrate = Client.objects.annotate(
            phone_count=Subquery(client_phones_with_phone_count.values("phone_count")),
            subscriber_gdpr_consent=Coalesce(Subquery(subscriber_emails.values("gdpr_consent")[:1]), False),
            subscriber_sms_gdpr_consent=Coalesce(Subquery(subscriber_sms_phones.values("gdpr_consent")[:1]), False),
            email_in_subscribers=Exists(subscriber_emails),
            subscriber_id=Coalesce(Subquery(subscriber_emails.values("id")[:1]), -1),
            phone_in_subscriber_sms=Exists(subscriber_sms_phones),
            subscriber_sms_id=Coalesce(Subquery(subscriber_sms_phones.values("id")[:1]), -1),
            phone_in_users=Exists(user_phones),
            email_in_users=Exists(user_emails),
        )

        for client in clients_to_migrate:
            if client.phone_count > 1:
                duplicated_client_phones.append(client)
                continue

            elif (client.email_in_users and client.email_in_subscribers) or (
                client.phone_in_users and client.phone_in_subscriber_sms
            ):
                continue

            else:
                if all((client.email_in_subscribers, not client.phone_in_users, not client.email_in_users)):
                    users_to_create.append(
                        User(email=client.email, phone=client.phone, gdpr_consent=client.subscriber_gdpr_consent)
                    )
                elif all((client.email_in_subscribers, client.phone_in_users, not client.email_in_users)):
                    subscriber_conflicts.append(Subscriber(id=client.subscriber_id, email=client.email))

                elif all((client.phone_in_subscriber_sms, not client.phone_in_users, not client.email_in_users)):
                    users_to_create.append(
                        User(email=client.email, phone=client.phone, gdpr_consent=client.subscriber_sms_gdpr_consent)
                    )
                else:
                    subscriber_sms_conflicts.append(SubscriberSMS(id=client.subscriber_sms_id, phone=client.phone))

        subscribers_without_email_in_clients = Subscriber.objects.exclude(email__in=clients_to_migrate.values("email"))

        for subscriber in subscribers_without_email_in_clients:
            users_to_create.append(User(email=subscriber.email, gdpr_consent=subscriber.gdpr_consent))

        subscriber_sms_without_phone_in_client = SubscriberSMS.objects.exclude(
            phone__in=clients_to_migrate.values("phone")
        )

        for subscriber_sms in subscriber_sms_without_phone_in_client:
            users_to_create.append(User(phone=subscriber_sms.phone, gdpr_consent=subscriber_sms.gdpr_consent))

        duplicated_phones_count = save_objs_to_csv(
            file_path=os.path.join(settings.REPORTS_ROOT, "duplicated_client_phones.csv"),
            fields_to_save=("id", "phone", "phone_count"),
            objs=duplicated_client_phones,
        )
        logger.info(f"Saving csv completed, found {duplicated_phones_count} Records with duplicated client phones")

        subscriber_conflicts_found = save_objs_to_csv(
            file_path=os.path.join(settings.REPORTS_ROOT, "subscriber_conflicts.csv"),
            fields_to_save=("id", "email"),
            objs=subscriber_conflicts,
        )
        logger.info(f"Saving csv completed, found {subscriber_conflicts_found} Subscriber conflicts")

        subscriber_sms_conflicts_found = save_objs_to_csv(
            file_path=os.path.join(settings.REPORTS_ROOT, "subscriber_sms_conflicts.csv"),
            fields_to_save=("id", "phone"),
            objs=subscriber_sms_conflicts,
        )
        logger.info(f"Saving csv completed, found {subscriber_sms_conflicts_found} SubscriberSMS conflicts")

        if users_to_create:
            User.objects.bulk_create(users_to_create)

        logger.info(f"Created {len(users_to_create)} new Users")
