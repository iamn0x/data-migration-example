from django.db import models

from utils.db.fields import PhoneField
from utils.db.models import GDPRConsentModel, TimeCreationModel


class User(
    GDPRConsentModel,
    TimeCreationModel,
    models.Model,
):
    email = models.EmailField()
    phone = PhoneField()
