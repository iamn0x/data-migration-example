import factory
from factory import fuzzy

from apps.users.models import User


class UserFactory(factory.django.DjangoModelFactory):
    email = factory.Faker("email")
    gdpr_consent = fuzzy.FuzzyChoice([True, False])
    phone = fuzzy.FuzzyInteger(100000000, 999999999)

    class Meta:
        model = User
