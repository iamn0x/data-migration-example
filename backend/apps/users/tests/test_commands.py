import os
import shutil

from django.conf import settings
from django.core.management import call_command

import pytest

from apps.customers.tests.factories import ClientFactory
from apps.subscribers.tests.factories import (
    SubscriberFactory,
    SubscriberSMSFactory,
)
from apps.users.models import User
from apps.users.tests.factories import UserFactory


@pytest.mark.django_db
class TestMigrateToUserCommand:
    def teardown(self):
        shutil.rmtree(settings.REPORTS_ROOT)

    def test_duplicated_phone_report(self, django_assert_num_queries):
        phone = "111111111"
        count = 2
        clients = ClientFactory.create_batch(count, phone=phone)
        ClientFactory()  # not duplicated

        with django_assert_num_queries(3):
            call_command("migrate_to_users")

        with open(os.path.join(settings.REPORTS_ROOT, "duplicated_client_phones.csv"), "r") as file:
            expected_content = (
                f"id,phone,phone_count\n{clients[0].id},{phone},{count}\n{clients[1].id},{phone},{count}\n"
            )
            assert file.read() == expected_content

    def test_user_email_exists_in_subscriber(self, django_assert_num_queries):
        email = "email_to_omit@example.com"
        subscriber_to_omit = SubscriberFactory(email=email)
        ClientFactory(email=subscriber_to_omit.email)
        UserFactory(email=subscriber_to_omit.email)

        with django_assert_num_queries(3):
            call_command("migrate_to_users")

        assert User.objects.filter(email=email).count() == 1

    def test_client_email__eq__subscriber_email__not_existing_user(self, django_assert_num_queries):
        """Client.email == Subscriber.email
        not existing User.phone == Client.phone
        and User.email != Client.email

        -> New User with Client field values"""

        subscriber = SubscriberFactory()
        client = ClientFactory(email=subscriber.email)

        with django_assert_num_queries(4):
            call_command("migrate_to_users")

        assert User.objects.filter(
            email=client.email,
            phone=client.phone,
            gdpr_consent=subscriber.gdpr_consent,
        ).exists()

    def test_client_email__eq__subscriber_email__existing_user_phone__eq__client_phone(self, django_assert_num_queries):
        """Client.email == Subscriber.email
        existing User.phone == Client.phone
        and User.email != Client.email

        -> subscriber_conflicts.csv"""
        subscriber = SubscriberFactory()
        client = ClientFactory(email=subscriber.email)
        UserFactory(phone=client.phone)

        with django_assert_num_queries(3):
            call_command("migrate_to_users")

        with open(os.path.join(settings.REPORTS_ROOT, "subscriber_conflicts.csv"), "r") as file:
            expected_content = f"id,email\n{subscriber.id},{subscriber.email}\n"
            assert file.read() == expected_content

    def test_not_existing_client_email__eq__subscriber_email(self, django_assert_num_queries):
        """not existing Client.email == Subscriber.email

        -> Creates User without phone"""
        subscriber = SubscriberFactory()

        with django_assert_num_queries(4):
            call_command("migrate_to_users")

        assert User.objects.filter(
            email=subscriber.email,
            gdpr_consent=subscriber.gdpr_consent,
            phone="",
        ).exists()

    def test_user_phone_exists_in_subscriber_sms(self, django_assert_num_queries):
        phone = "123123123"
        subscriber_sms_to_omit = SubscriberSMSFactory(phone=phone)
        ClientFactory(phone=subscriber_sms_to_omit.phone)
        UserFactory(phone=subscriber_sms_to_omit.phone)

        with django_assert_num_queries(3):
            call_command("migrate_to_users")

        assert User.objects.filter(phone=phone).count() == 1

    def test_client_phone__eq__subscriber_sms_phone__not_existing_user(self, django_assert_num_queries):
        """Client.phone == SubscriberSMS.phone
        not existing User.email == Client.email
        and User.email != Client.email

        -> New User with Client field values"""

        subscriber_sms = SubscriberSMSFactory()
        client = ClientFactory(phone=subscriber_sms.phone)

        with django_assert_num_queries(4):
            call_command("migrate_to_users")

        assert User.objects.filter(
            email=client.email,
            phone=client.phone,
            gdpr_consent=subscriber_sms.gdpr_consent,
        ).exists()

    def test_client_phone__eq__subscriber_sms_phone__existing_user_email__eq__client_email(
        self, django_assert_num_queries
    ):
        """Client.email == SubscriberSMS.email
        existing User.email == Client.email
        and User.phone != Client.phone

        -> subscriber_sms_conflicts.csv"""
        subscriber_sms = SubscriberSMSFactory()
        client = ClientFactory(phone=subscriber_sms.phone)
        UserFactory(email=client.email)

        with django_assert_num_queries(3):
            call_command("migrate_to_users")

        with open(os.path.join(settings.REPORTS_ROOT, "subscriber_sms_conflicts.csv"), "r") as file:
            expected_content = f"id,phone\n{subscriber_sms.id},{subscriber_sms.phone}\n"
            assert file.read() == expected_content

    def test_not_existing_client_phone__eq__subscriber_sms_phone(self, django_assert_num_queries):
        """not existing Client.phone == SubscriberSMS.phone

        -> Creates User without email"""
        subscriber_sms = SubscriberSMSFactory()

        with django_assert_num_queries(4):
            call_command("migrate_to_users")

        assert User.objects.filter(
            phone=subscriber_sms.phone,
            gdpr_consent=subscriber_sms.gdpr_consent,
            email="",
        ).exists()
