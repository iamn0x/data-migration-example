import environ

env = environ.Env()
env.read_env("/run/secrets/django")

from config.settings.base import *  # noqa pylint: disable=unused-wildcard-import

SHELL_PLUS_POST_IMPORTS = (
    "from apps.customers.factories import ClientFactory",
    "from apps.subscribers.factories import SubscriberFactory, SubscriberSMSFactory",
    "from apps.users.factories import UserFactory",
)
