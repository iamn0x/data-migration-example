import environ

env = environ.Env()
env.read_env("/run/secrets/django")

from config.settings.base import *  # noqa pylint: disable=unused-wildcard-import

REPORTS_ROOT = os.path.join(ROOT_DIR, "test-reports")
