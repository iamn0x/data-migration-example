import csv
from typing import Union

from django.db.models import QuerySet


def save_objs_to_csv(file_path: str, fields_to_save: Union[list, tuple], objs: Union[list, tuple, QuerySet]):
    count = 0
    with open(file_path, "w") as my_file:
        writer = csv.writer(my_file)
        writer.writerow(fields_to_save)
        for data_item in objs:
            writer.writerow([getattr(data_item, field_name) for field_name in fields_to_save])
            count += 1

    return count
