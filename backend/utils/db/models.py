from django.db import models


class GDPRConsentModel(models.Model):
    gdpr_consent = models.BooleanField("General Data Protection Regulation consent")

    class Meta:
        abstract = True


class TimeCreationModel(models.Model):
    created_date = models.DateTimeField("Date of creation", auto_now_add=True)

    class Meta:
        abstract = True
