from django.db import models


class PhoneField(models.CharField):
    def __init__(self, *args, max_length=15, **kwargs) -> None:
        super().__init__(*args, max_length=max_length, **kwargs)
