# Data Migration Example

Small Django project with custom Django Management Command to make migration to User model from two other apps based on conditions:

- Omits Clients with duplicated phone number:
    - Omits Subscribers whose e-mails exist in the User table:

        - If there's Client.email == Subscriber.email
        and not exists User.phone == Client.phone
        and User.email != Client.email -> Creates new User based on Client model

        - If there's Client.email == Subscriber.email
        and User.phone == Client.phone
        and User.email != Client.email -> Saves Subscriber ID and email to 'subscriber_conflicts.csv'

        - If there's not Client.email == Subscriber.email -> Creates new User without phone

    - Omits SubscriberSMS whose phones exist in the User table:

        - If there's Client.phone == Subscriber.phone
        and not exists User.email == Client.email
        and User.phone != Client.phone -> Creates new User based on Client model

        - If there's Client.phone == Subscriber.phone
        and User.email == Client.email
        and User.phone != Client.phone -> Saves Subscriber ID and phone to 'subscriber_sms_conflicts.csv'

        - If there's not Client.phone == Subscriber.phone -> Creates new User without email


### Local Development Server

```bash
$ cp ./.envs/.django.example ./.envs/.django
$ cp ./.envs/.postgres.example ./.envs/.postgres
$ cp ./.secrets/.django.example ./.secrets/.django
$ docker-compose up --build
```


### Run Tests

There's no need to add extra arguments to pytest, it adopts things such as django settings module, database reuse, coverage and more.

```bash
$ docker-compose run --rm django /bin/sh pytest
```


### Django Service Logs

To get django logs in console run:

```bash
$ docker-compose logs -f --tail=1000 --no-log-prefix django
```


### Attach to container

To manually use example command (or other things in django container):

```bash
$ docker-compose run --rm django /bin/sh
$ python manage.py migrate_to_users
```


### Clear Services Data

To stop and clear all services data (volumes and images) run:

```bash
$ docker-compose rm -fsv
$ docker rmi noxitrs/data-migration-example:local
$ docker rmi postgres:14
```

---