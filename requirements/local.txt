-r ./base.txt

# Development - Code Quality
isort==5.10.1  # https://github.com/pycqa/isort
black==22.1.0  # https://github.com/psf/black
flake8==4.0.1  # https://github.com/pycqa/flake8
pylint==2.12.2  # https://github.com/PyCQA/pylint
pylint-django==2.5.2  # https://github.com/PyCQA/pylint-django
bandit==1.7.2  # https://github.com/PyCQA/bandit

# Development - Test Tools
factory-boy==3.2.1  # https://github.com/FactoryBoy/factory_boy
pytest==7.0.0  # https://github.com/pytest-dev/pytest
pytest-django==4.5.2  # https://github.com/pytest-dev/pytest-django
pytest-sugar==0.9.4  # https://github.com/Teemu/pytest-sugar/
pytest-mock==3.7.0  # https://github.com/pytest-dev/pytest-mock/

# Coverage - Test Tools
coverage==6.3.1  # https://github.com/nedbat/coveragepy
pytest-cov==3.0.0  # https://github.com/pytest-dev/pytest-cov
