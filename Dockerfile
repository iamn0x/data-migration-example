FROM python:3.9-alpine

ARG user_id=1000
ARG group_id=1000
ENV USER_ID=$user_id
ENV GROUP_ID=$group_id

ARG environment=local
ENV ENVIRONMENT=$environment

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

COPY requirements /srv/requirements

RUN apk --no-cache --upgrade add gettext && \
    apk add --no-cache --virtual builddeps gettext gcc musl-dev && \
    pip install --no-cache-dir -r /srv/requirements/${ENVIRONMENT}.txt && \
    apk del builddeps && rm -rf /srv/requirements

COPY scripts /srv/scripts
COPY backend /srv/app

RUN addgroup -g ${GROUP_ID} app_group && \
    adduser --uid ${USER_ID} --system --ingroup app_group app_user && \
    chown -R app_user:app_group /srv/

USER app_user

WORKDIR /srv/app